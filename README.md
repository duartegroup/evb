# EVB
## 1. Overview 
The aim of this tutorial is to provide you with a general overview about how to perform Free Energy Perturbation/ Umbrella Sampling (FEP/US) Empirical Valence Bond (EVB) simulations of chemical reactions utilizing the software Q. Due to the complexity of the task and time constraints, this tutorial will just cover the crucial aspects of the general procedure, including topology generation, FEP file generation, FEP/US simulations and group contribution calculations.

Other aspects such as parameterization, structure processing, substrate docking, setting protonation states, equilibrations, etc. will not be covered and the reader is encouraged to master them if the aim is to use the approach in a scientific project. Below, we provide some reference that will help the reader to gain a deeper understanding of complete protocol.

By the end of this tutorial, you should be able to understand the fundamental concepts behind EVB calculations, the structure of Q program and how to set up and analyze FEP/US simulations yourself. 
 
### 1.1 Requirements & Software
A functional understanding of the Unix/Linux environment and specifically the command line, is prerequisite for successfully completing this tutorial, therefore we strongly recommend that those new to Unix/Linux acquaint themselves with the basics beforehand (example tutorial: http://www.ee.surrey.ac.uk/Teaching/Unix/unix1.html). Instructions regarding accessing the remote computer cluster will be provided separately.

All FEP/US EVB simulations will be performed using the software package Q. this software allows the calculation of properties such as solvation free energies, binding affinities, and reaction free energies, which is the topic of this tutorial, Q is developed and maintained by the Kamerlin and Åqvist groups at Uppsala University, it is an open source software (GPLv2) available free of charge from the online repository GitHub. 

To complement the bare-bones nature of Q and make our lives a bit easier, we will also make use of the python scripts provided in the software package Qtools https://github.com/mpurg/qtools. These scripts will simplify the tasks required to successfully carry out this tutorial: parameter conversion, input generation, simulation analysis, etc. Again, please see Addendum for installation instructions.

The final, but crucial software requirement is VMD (Visual Molecular Dynamics), allowing us to visualize and analyze our simulations. VMD can be obtained free of charge (registration required) here: http://www.ks.uiuc.edu/Research/vmd/. It will also be installed on the node

### 1.2 Useful References
* A. Warshel, R.M. Weiss, J. Am. Chem. Soc., 1980, 102, 6218. 
* J. Åqvist, and A. Warshel Chem. Rev. 1993, 93, 2523. 
* Hong, G., Rosta, E. and Warshel, A. J. Phys. Chem B, 2006 110, 19570.
* Warshel, A. (1991). Computer Modeling of Chemical Reactions in Enzymes and Solutions (New York: Wiley).
* Marelius, K. Kolmodin, I. Feierberg, J. Åqvist, J. Mol.  Graph. Mod. 1998, 16, 213.
* P. Bauer, A. Barrozo, B. Amrein, M. Esguerra, P. B. Wilson, D. T. Major, J. Åqvist and S. C. L. Kamerlin, SoftwareX 2018  https://github.com/qusers/Q6 
* F . Duarte, B. A. Amrein, D. Blaha-Nelson and S. C. L. Kamerlin, BBA - General Subjects 2015, 1850, 954. 
* F. Duarte, S. C. L. Kamerlin (Editors). From Physical Chemistry to Chemical Biology: Theory and Applications of the Empirical Valence Bond Approach. John Wiley & Sons. 2017. 
* Purg, M., Kamerlin, S.C.L., In review in Methods in Enzymology (Vol 607: Phosphatases)
* Purg, M., Elias, M. and Kamerlin, S.C.L. J. Am. Chem. Soc.  2017, 139, 17533.
*  Muegge, I., Tao, H. and Warshel, A.,. Protein engineering, 1997, 10, 1363.
 
### 2. Case studies
In this tutorial we will study two systems: 

* The uncatalyzed SN2 reaction in gas and aqueous phase.
* The hydrolysis of organophosphate compounds in squid diisopropyl-fluorophosphatase (DFPase). https://github.com/mpurg/methodsenz

This tutorial will guide you through several steps, from the setup of the system to be studied, to the analysis of the calculations. It will be divided into five parts: 

* Setting up the systems
* Creating FEP files
* Set up of FEP/US simulation 
* _In silico_ mutagenesis
* Visualization and analysis

#### 2.1 SN2 Reaction 
##### 2.1.1 Building a topology
The first task in this tutorial is fairly straightforward – building a Q topology file. The topology file contains information about the system required for running classical MD simulations in Q, which includes coordinates, bonding patterns and force field parameters. It is built using the Qprep6 tool that comes with the default installation of Q6. Note that all the necessary files to build the topology are provided with the tutorial.

Go to the folder 0-top. There you will find two subfolders and one input file. If you type ls (or ll -t), you will be able to see all the files (folders are in bold) in this folder:
```bash
$ ls –l
0-ff
prep.inp
probr_cl.pdb
```

Take a look at the PDB file called probr_cl .pdb. It contains the xyz coordinates of the system and residue/atom names. 
It is essential to have the correct order of the atoms in order for the program to match them to the corresponding parameters of the force field. These parameters can be found in the folder 0-ff (extension .lib and .prm) 
We will use the Qprep6 program to set up our system. The input file (prep.inp), contains the instructions that will be given to Qprep6 to create a topology file. Here is an example of the input file to be used for this task: 

It tells the program which starting structure to use (readpdb probr_cl.pdb) and where to find the parameters (readlib/readprm) It also contains instruction for solvation the system. In this case, that line is commented and the generated topology (.top) will not contain any solvent. Finally, the input will write the topology file and a new PDB file You can open the new PDB in a molecular visualization software to see that no solvation has been included (you can also uncomment that line).
Now build the topology using Qprep6
$ Qprep6 < prep.inp > prep.out
The output qprep.out of a successful build should end with “PDB file successfully written”, with Qprep6 writing the following two files to disk: probr_cl.top, and probr_cl_start.pdb.
Copy probr_cl_start.pdb, which contains the processed coordinates, to your local computer and visualize it using VMD. If you’re not familiar with VMD, here are some useful shortcuts:
```bash
=                reset view 
	T, R, C       translate, rotate, center (mouse-click to select center atom)
	1, 2, 3, 4   label (atom, bond, angle, dihedral)
                   use Graphics->Labels to display and plot
```

#### 2.1.2.Molecular Dynamics
Using the newly created topology we will perform a procedure called relaxation using Qdyn6. It basically consists of running MD simulations to allow our system to find a minimum in its potential energy at the temperature we wish to study. We will go to folder 1-relax and perform a set of MD runs, going from 1 K up to 300 K.
Copy the topology from where you created it (0-top to the 1-relax directory. Within this directory, you will find three input files (relax_00X.inp). Those are the inputs used for the full relaxation procedure, and should be run sequentially. To run them we will type: 
bash run_relax_q.sh

Check the progress of the simulation:
$ grep –E “summary|terminated” relax_xxx.out | tail

At the end of your relaxation process (three ‘OK’ should appear), you should have three .log, .dcd and .re files. relax_003.re file will be used to launch your EVB calculations. In order to make sure that all relaxation process was accomplished, check the output file from your last input. At the end of the file you should see that Q terminated normally. You can visualize the files in VMD (load the pdb and then the dcd file)
While the simulation is running in the background, try to familiarize yourself with the structure of the Qdyn6 input. It is well commented, however, we encourage you to look up the keywords in the Q manual as well. 
Relax_003.inp
```bash
[md]
steps                       100000
temperature                 300
stepsize                    1
bath_coupling               100

[cut-offs]
q_atom                     99

[files]
topology                    probr_cl.top
restart                     relax_002.re
final                       relax_003.re
trajectory                  relax_003.dcd
fep                         probr_cl.fep

[lambdas]
1.00  0.00

[sequence_restraints]
1 6 0.1 0 0
11 12 0.1 0 0

[distance_restraints]
1 11 0.0 3.5 3.0 2
1 12 0.0 3.5 3.0 1
```
We will go through some of these sections:

**[MD]**

**steps** stands for the total number of MD steps to be performed, 

**Stepsize** is the size of the step in fs, 

**Temperature** Number in K, 

**bath-coupling** is related to the thermostat used to ensure constant temperature. 

**[files]** Here we specify topology and FEP file to be used, as well as the names for the trajectory (.dcd) and restart (.re) files to be written. 

**[lambdas]** This section related to the free energy perturbation calculation to be performed between the reactant and the product state. 1.0 0.0 stands for the system 100 % at the state 1 (reactant), and 0 % at the state two (product). 

**[position restraints]** restraints are used to ensure that the heating is made in a smooth manner. The first two numbers correspond to the atom numbers in the PDB generated at the setup step. For further information, please refer to the Q manual. 

**[distance restraints]** A weak distance restraint is also used to ensure the fragments are kept in the center of the sphere. 

### 2.1.3 Generating FEP file
To calculate relative free energies of any process using the FEP methodology, we need to define the changes that occur during the transformation from one state to the other. In Q, this is defined in the so-called FEP-file, which, just to give you a brief overview, consists of the following sections (see Q manual p.25-32 for details):
 
 -	[FEP]	# used to define number of states, etc
 
 -	[atoms]	# define Q atoms (i.e. reactive, EVB, FEP) and map PDB indices to Q indices
 
 -	[atom_types]	# VdW parameters
 
 -	[change_atoms] 	# changes in VdW parameters
 
 -	[change_charges]	# changes in partial charges
 
 -	[bonds]	# bonding parameter definitions
 
 -	[change_bonds]	# changes in bonding
   
 -	[angles] … …	# analogous to bonds

The FEP files can in principle be generated manually, however, this proves to be a very laborious and somewhat error-prone task. Thus in order to save some time and limit the number of typos, we will make use of the handy qtools utility q_makefep.py. In the next section we will describe in detail how to use this tool.

Now let us take a look at each part of the FEP file: 

The first section indicates the number of sates to be used and the atom to be part of the Q-region (first Q-atom ID and second PDB number of the atom). 
```bash
[FEP]
states 2
[atoms]
#Q index     PDB index 
1            1
2            2 
3            3 
4            4 
5            5 
6            6 
7            11 
8            12 
```
The next section gives the name of each atom type, vdW (columns two and three), soft pair (columns four and five), the 1-4 interactions (columns six and seven), and the mass of the atom. The soft pair interaction is used between reacting fragments, in order to reduce the high repulsion from the Lennard-Jones potential as the atoms come close together to form bonds. Details about it can be found at the Q manual. 
```bash
[atom_types]
prb_C1      944.5180   22.0296  91.0   2.5   667.8751    15.5773    12.011 
prc_Cl11  1692.2485    43.0554  90.0   2.5  1196.6004    30.4447     35.453 
cl-_Cl1   5099.0001    59.1621  90.0   2.5  3605.5376    41.8339     35.453 
```
Then the partial charges corresponding to the states 1 (reactant) and 2 (product) for the EVB calculation are indicated. The first column contains the Q-atom ID, and the last two columns are the charges for the EVB states 1 and 2. 
```bash
[change_charges]
1  -0.3022    -0.1984     #    PRB.C1     dq= 0.1038 
2   0.1441     0.1206     #    PRB.H2     dq=-0.0235 
3   0.1441     0.1206     #    PRB.H3     dq=-0.0235 
```
Sometimes atoms can change their orbital properties, as it happens between sp2 and sp3 C atoms, or bonded and unbounded halogens. Their vdW properties will change, and it is here where we account for this. It works like the change in charges. 
```bash
[change_atoms]
1          prb_C1       prc_C1           #    PRB.C1     !
7          prb_Br11     br-_Br1          #    PRB.Br11   !
8          cl-_Cl1      prc_Cl11         #    CL-.Cl1    ! 
```
Then we add the pair of atoms where the soft potential will act. Again, we are using the Q-atom IDs. Note that here we are only using the atoms that breaking or forming bonds. 
```bash
[soft_pairs]
1     7       # prb_C1-prb_Br11     
1     8       # prc_C1-prc_Cl11     

[bond_types]
1    66.0  1.58  1.94   # prb_C1-prb_Br11      
2    78.0  1.51  1.80   # prc_C1-prc_Cl11       

[change_bonds]
1   11     1    0   # 1.C1-1.Br11 prb_C1-prb_Br11 None                
1   12     0    2   # 1.C1-2.Cl1 None prc_C1-prc_Cl11     
```

These two sections are related to bond modifications. The bond types with three columns have changes in the spring constant (second column) and length (third column, in Å) of a covalent bond, and they are represented by a harmonic potential. The four columns in the bond type section provide the parameters for a Morse potential, which we use to represent the cleavage and formation of bonds. On the following section, we assign the type of bonds for every pair of atoms we are interested in. This time we use the PDB-atom ID for the pair of atoms, and two more columns, representing which type of bond will be used for the two states. Note that for cases where a bond is inexistent a zero is used. By analyzing the FEP file, can you tell which bond is being broken, and which one is being formed? 

For the angles and torsions, the procedure is analogous. The shape of the potentials follow the OPLS-AA force field convention. If you would like to know more, please refer to the Q manual. 

Finally, although being an old command, this part enables the printing of distances between two atoms in the output file. Here, the columns three and four have the Q-atom IDs of the atoms we would like information about. 
```bash
[off_diagonals]
1   2       1     7  0  0  # prb_C1-prb_Br11     
1   2       1     8  0  0  # prc_C1-prc_Cl11     
```

### 2.1.4 EVB FEP/US simulations

With the system relaxed, you can now begin the EVB calculations. Go to the 2-fep folder. You will see that it contains the following directory:
1-RS_000
The directory contains 51 input files corresponding to the EVB run. As you have learned from the lecture, EVB is a FEP procedure in which the energies used for the free energy calculation are a mix between the energies of the two or more states. 
In order to run such calculation, we need to have the FEP, topology file and the necessary restart files from the relaxation step:
```
cp ../../1-relax/relax_003.re  cont_relax_003.re
cp ../../1-relax/relax_003.re cont_relax_003.re.rest
cp ../../1-relax/*top .
```

The files must be executed sequentially, starting with equil_000_1.000 and then followed by the fep*inp files (from fep_000_1.000.inp, which corresponds to our reactant state (lambdas set as 1.0  and 0.0), going all the way to fep_050_0.000.inp, the file corresponding to the product state (lambdas 0.0 and 1.0). 
Use the submission script available at each of the folders. 
``` bash run_feps_q.sh 
```

### 2.1.5 Analyzing EVB Run and Calibrating the PES 
FEP profiles are calculated using the tool Qfep6, by providing the unknown EVB parameters H12 and gas-shift. Alternatively, we can use the qtools wrapper q_mapper.py, which will create the inputs, call Qfep6, and analyze the outputs in one go.

H12 and the gas-shift used here have been pre-calibrated to reproduce the ∆G‡ and ∆G0 of a reference reaction. 

H12: 76.543

Gas-shift: 2.345 

In some cases, when experimental data of similar systems are available one can also use them and obtain the corresponding activation barriers from the rate constants. Then it is a matter to find a set of empirical parameters that can adjust our EVB parabola to reproduce the results from such studies. Our values are: ∆G‡ = 13.0 kcal/mol and ∆G0= -5.4 kcal/mol. 
Go into 2-fep and calculate the free-energy profiles using q_mapper.py 
```$ q_mapper.py –h
Required:
  hij                   Hij coupling constant
  alpha                 state 2 shift (alpha)

Optional:
  --nt NTHREADS         Number of threads (default = 1)
  --bins GAP_BINS       Number of gap-bins (default=50).
  --skip POINTS_SKIP    Number of points to skip in each frame (default=100).
  --min MINPTS_BIN      Minimum points for gap-bin (default=10).
  --temp TEMPERATURE    Temperature (default=300.00).
  --dirs MAPDIRS [MAPDIRS ...]
                        Directories to map (default=all subdirs in cwd or
                        current dir)
  --out OUTFILE         Logfile name (default=q_mapper.log).
  --qfep_exec QFEP_EXEC
                        qfep5 executable path (default=).
```

Note that the coupling or non-diagonal elements of the Hamiltonian, can be a constant value of in more general terms of the form

Hij=exp(−μrij ), 

where $r_ij$ is the distance between the atoms composing the two extremes of the reaction. For simplicity here we will use a constant value.  

```
$ q_mapper.py 76.543 2.345 --bins 50 --skip 10 --min 1 --temp 298.15 
```

This will generate files q_mapper.log and qfep.out 

```
q_mapper.log
Analysis Stats:
#                 Mean      Std.dev    Median    Std.error       N
dG*              13.34        nan      13.34        nan          1
dG0              -5.58        nan      -5.58        nan          1
dG_lambda        -9.44        nan      -9.44        nan          1
```

Parts of the qfep.out file produced are shown below: 

```
# Part 0: Average energies for all states in all files
# file state pts lambda EQtot EQbond  EQang EQtor EQimp  EQel EQvdW Eel_qq EvdW_qq Eel_qp  EvdW_qp
--> Name of file number   1:                                                     
fep_000_1.000.en 1 1949 1.00 -74.97 -65.92  0.25 0.05  0.00 -9.22 -0.40  -7.17  -0.19  -2.05 -0.21 
fep_000_1.000.en 2 1949 0.00  92.97  -6.27 78.38 0.91  0.00 -7.89  27.84 -7.14  25.92  -0.75  1.93 
Name of file number   2:                                                     
fep_001_0.980.en 1 1949 0.98 -74.90 -65.87 0.19  0.05  0.00 -9.14 -0.41  -7.13  -0.19  -2.01 -0.21 
```

Part 0 shows a summary of the contributions to free energy, from bond to van der Waals contributions, for both states at different stages of the FEP procedure (i.e. different lambdas). 

```
# Part 1: Free energy perturbation summary:

# Calculation for full system
# lambda(1)      dGf sum(dGf)      dGr sum(dGr)     <dG>
   1.000000    0.000    0.000   -3.350   12.881    0.000
   0.980000    3.401    3.401   -3.275   16.231    3.375
   0.520000    0.763   60.469    0.074   70.531   59.059
   0.500000    0.363   60.831    0.556   70.457   59.204
   0.480000   -0.110   60.722    0.952   69.901   58.871
```

Part 1 shows how the free energy is built, both summing from lambda 1.00 to 0.00 (and backwards, dGf and dGb respectively. The last column contains the average between the forward and backward profile. Here you can spot where the transition state (TS) is located. Often the TS is found at the lambda 0.50000, although this is not always necessarily true. Later we will analyze the geometry of our system at the TS. Thus, keep in mind that you will find which lambda, and consequently which fep_XXX.log, we should look in order to get the TS distances. 
```
# Part 2: Reaction free energy summary:
# Lambda(1)  bin Energy gap      dGa     dGb     dGg    # pts    c1**2    c2**2
   1.000000    1    -181.57     0.00   178.60   -28.21     66    0.881    0.119
   1.000000    2    -173.60     0.00   170.86   -29.11    998    0.874    0.126
   1.000000    3    -165.62     0.00   164.28   -29.83    876    0.869    0.131
   0.980000    2    -173.60    -0.06   170.63   -29.24    444    0.873    0.127
```
Part 2 provides information about lambda values, number of bins, energy gap between states 1 and 2 for every lambda and finally the constants c12 and c22, which correspond to the coefficients of the mixing of the wavefunctions for states 1 and 2 (ψg = c1ψ1 + c2ψ2). 

```
# Part 3: Bin-averaged summary:
# bin  energy gap  <dGg> <dGg norm> pts  <c1**2> <c2**2> <r_xy>
   1   -181.57   -28.21     8.08     66   0.881   0.119   1.943
   2   -173.60   -29.15     7.14   1454   0.874   0.126   1.953
   3   -165.62   -29.97     6.32   4723   0.867   0.133   1.968
   4   -157.64   -30.53     5.76   6834   0.859   0.141   1.997
   5   -149.66   -30.71     5.58   5437   0.850   0.150   2.027
   6   -141.69   -30.53     5.76   4590   0.840   0.160   2.057
   7   -133.71   -30.06     6.23   3468   0.829   0.171   2.087
```
Here, we can extract the free energy profile (dGg norm) plotted against the energy gap as the reaction coordinate. From here you can extract the activation free energy, as well as the free energy difference between reactant and product states. Moreover, we also see the bins and their corresponding center in the energy gap axis. 
There are plenty of new concepts, so don’t worry you could not grasp all of them. It is just a matter of experience.  
You can visualize your trajectory in VMD:
```
vmd ../../0-topol/probr_cl_start.pdb fep_0*dcd
```

Also you can extract your activation barrier from part 3 of the output:
If you have some time left, try to include solvent in your simulation and see its effect on the activation barrier. Is that in line with your expectation?  

